using Godot;

public partial class RotationScript : Sprite2D
{
    [Export]
    public float Speed = 300; // How fast the player will move (pixels/sec).

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(double delta)
    {
        var velocity = Vector2.Zero;

        if (Input.IsKeyPressed(Key.Right))
        {
            velocity.X += 1;
        }

        if (Input.IsKeyPressed(Key.Left))
        {
            velocity.X -= 1;
        }

        if (Input.IsKeyPressed(Key.Down))
        {
            velocity.Y += 1;
        }

        if (Input.IsKeyPressed(Key.Up))
        {
            velocity.Y -= 1;
        }

        Position += ((float)delta * Speed) * velocity;
    }
}
